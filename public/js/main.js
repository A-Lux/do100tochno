$('.ins-slider').slick({
	centerMode: true,
	centerPadding: '233px',
	slidesToShow: 4,
	arrows: false,
	responsive: [
		{
			breakpoint: 1400,
			settings: {
				slidesToShow: 3,
				centerPadding: '233px',
				}
			},
		{
		breakpoint: 992,
		settings: {
			slidesToShow: 3,
			centerPadding: '40px',
			}
		},
	  {
		breakpoint: 992,
		settings: {
		  slidesToShow: 3,
		  centerPadding: '40px',
		}
	  },
	  {
		breakpoint: 768,
		settings: {
		  slidesToShow: 2,
		  centerPadding: '38px',
		}
	  },
	  {
		breakpoint: 575,
		settings: {
		  slidesToShow: 1,
		  centerPadding: '38px',
		}
	  }
	]
  });


$(document).ready(function(){$('nav a').bind("click",function(e){var anchor=$(this);$('html, body').stop().animate({scrollTop:$(anchor.attr('href')).offset().top},1000);e.preventDefault();});});




ymaps.ready(init);


var myMap, place1, place2, place3, place4, place5, place6, place7, place8, place9, place10, place11, place12, place18, place14, place15, place16, place17, myAction;

function init(){     
    myMap = new ymaps.Map ("map", {
        center: [53.521570, 49.428038],
        zoom: 17,
        duration: 50
    });
	myMap.behaviors.disable('scrollZoom');
    
	place1 = new ymaps.Placemark([53.521570, 49.428038],  { content: 'Т Весна', balloonContent: 'Т Весна, Горького 92 А/1' });
    place2 = new ymaps.Placemark([53.530370, 49.275684], { content: 'Т Восход', balloonContent: 'Т Восход Революционная  20' });
	place3 = new ymaps.Placemark([53.532939, 49.300486],  { content: 'Т Десятка', balloonContent: 'Т Десятка, Степана Разина 9A' });
	place4 = new ymaps.Placemark([53.507831, 49.290748],  { content: 'Т Детский Мир', balloonContent: 'Т Детский Мир, Степана Разина 60' });
	place5 = new ymaps.Placemark([53.507477, 49.403433],  { content: 'Т Журавль', balloonContent: 'Т Журавль, Мира 56' });
	place6 = new ymaps.Placemark([53.510476, 49.260807],  { content: 'Т Захар', balloonContent: 'Т Захар, Королева 18а' });
	place7 = new ymaps.Placemark([53.524273, 49.318336],  { content: 'Т Изюм', balloonContent: 'Т Изюм, Свердлова 3Б' });
	place8 = new ymaps.Placemark([53.540383, 49.344126],  { content: 'Т Каретный', balloonContent: 'Т Каретный, Тополиная 24A' });
	place9 = new ymaps.Placemark([53.481138, 49.476026],  { content: 'Т Кунеевский', balloonContent: 'Т Кунеевский, Лизы Чайкиной 50A' });
	place10 = new ymaps.Placemark([53.505662, 49.415713],  { content: 'Т Ленинградская 53A', balloonContent: 'Т Ленинградская 53A' });
	place11 = new ymaps.Placemark([53.507724, 49.414078],  { content: 'Т Мира 67', balloonContent: 'Т Мира 67' });
	place12 = new ymaps.Placemark([53.532992, 49.326843],  { content: 'Т Николаевский', balloonContent: 'Т Николаевский, Автостроителей, 68A' });
	place18 = new ymaps.Placemark([53.529626, 49.326932],  { content: 'Т Новинка', balloonContent: 'Т Новинка, Автостроителей 80 А к1' });
	place14 = new ymaps.Placemark([53.527126, 49.291952],  { content: 'Т Сорренто-Оптима', balloonContent: 'Т Сорренто-Оптима, Свердлова 15' });
	place15 = new ymaps.Placemark([53.526639, 49.274174],  { content: 'Т Ст.торговый', balloonContent: 'Т Ст.торговый, Революционная  28/1' });
	place16 = new ymaps.Placemark([53.521484, 49.294530],  { content: 'Т Телецентр', balloonContent: 'Т Телецентр, Степана Разина 26 А' });
	place17 = new ymaps.Placemark([53.524669, 49.307017],  { content: 'Т Туполева', balloonContent: 'Т Туполева, Туполева 3а' });



    

    place = new ymaps.Placemark([],  { content: ',', balloonContent: '' });
    
    
    
    place13 = new ymaps.Placemark([],  { content: '', balloonContent: '' });

    
    myMap.geoObjects.add(place1);
    myMap.geoObjects.add(place2);
    myMap.geoObjects.add(place3);
    myMap.geoObjects.add(place4);
    myMap.geoObjects.add(place5);
    myMap.geoObjects.add(place6);
    myMap.geoObjects.add(place7);
    myMap.geoObjects.add(place8);
    myMap.geoObjects.add(place9);
    myMap.geoObjects.add(place10);
    myMap.geoObjects.add(place11);
    myMap.geoObjects.add(place12);
    myMap.geoObjects.add(place18);
    myMap.geoObjects.add(place14);
    myMap.geoObjects.add(place15);
    myMap.geoObjects.add(place16);
    myMap.geoObjects.add(place17);

    
    myAction1 = new ymaps.map.action.Single({
          center: place1.geometry.getCoordinates(),
          zoom: 17,
          duration: 1000
    });
    myAction2 = new ymaps.map.action.Single({
          center: place2.geometry.getCoordinates(),
          zoom: 17,
          duration: 1000
    });
	myAction3 = new ymaps.map.action.Single({
		center: place3.geometry.getCoordinates(),
		zoom: 17,
		duration: 1000
  	});
	myAction4 = new ymaps.map.action.Single({
		center: place4.geometry.getCoordinates(),
		zoom: 17,
		duration: 1000
  	});
	myAction5 = new ymaps.map.action.Single({
		center: place5.geometry.getCoordinates(),
		zoom: 17,
		duration: 1000
  	});
	myAction6 = new ymaps.map.action.Single({
		center: place6.geometry.getCoordinates(),
		zoom: 17,
		duration: 1000
  	});
	  myAction7 = new ymaps.map.action.Single({
		center: place7.geometry.getCoordinates(),
		zoom: 17,
		duration: 1000
  	});
	  myAction8 = new ymaps.map.action.Single({
		center: place8.geometry.getCoordinates(),
		zoom: 17,
		duration: 1000
  	});
	  myAction9 = new ymaps.map.action.Single({
		center: place9.geometry.getCoordinates(),
		zoom: 17,
		duration: 1000
  	});
	  myAction10 = new ymaps.map.action.Single({
		center: place10.geometry.getCoordinates(),
		zoom: 17,
		duration: 1000
  	});
	  myAction11 = new ymaps.map.action.Single({
		center: place11.geometry.getCoordinates(),
		zoom: 17,
		duration: 1000
  	});
	  myAction12 = new ymaps.map.action.Single({
		center: place12.geometry.getCoordinates(),
		zoom: 17,
		duration: 1000
  	});
	  myAction18 = new ymaps.map.action.Single({
		center: place18.geometry.getCoordinates(),
		zoom: 17,
		duration: 1000
  	});
	  myAction14 = new ymaps.map.action.Single({
		center: place14.geometry.getCoordinates(),
		zoom: 17,
		duration: 1000
  	});
	  myAction15 = new ymaps.map.action.Single({
		center: place15.geometry.getCoordinates(),
		zoom: 17,
		duration: 1000
  	});
	  myAction16 = new ymaps.map.action.Single({
		center: place16.geometry.getCoordinates(),
		zoom: 17,
		duration: 1000
  	});
	  myAction17 = new ymaps.map.action.Single({
		center: place17.geometry.getCoordinates(),
		zoom: 17,
		duration: 1000
  	});
}

$("#place1").click(function(){
    myMap.action.execute(myAction1);
});
$("#place2").click(function(){
    myMap.action.execute(myAction2);
});
$("#place3").click(function(){
    myMap.action.execute(myAction3);
});
$("#place4").click(function(){
    myMap.action.execute(myAction4);
});
$("#place5").click(function(){
    myMap.action.execute(myAction5);
});
$("#place6").click(function(){
    myMap.action.execute(myAction6);
});
$("#place7").click(function(){
    myMap.action.execute(myAction7);
});
$("#place8").click(function(){
    myMap.action.execute(myAction8);
});
$("#place9").click(function(){
    myMap.action.execute(myAction9);
});
$("#place10").click(function(){
    myMap.action.execute(myAction10);
});
$("#place11").click(function(){
    myMap.action.execute(myAction11);
});
$("#place12").click(function(){
    myMap.action.execute(myAction12);
});
$("#place18").click(function(){
    myMap.action.execute(myAction18);
});
$("#place14").click(function(){
    myMap.action.execute(myAction14);
});
$("#place15").click(function(){
    myMap.action.execute(myAction15);
});
$("#place16").click(function(){
    myMap.action.execute(myAction16);
});
$("#place17").click(function(){
    myMap.action.execute(myAction17);
});


$( '.menu>li, .submenu>li' ).click(function(e) {
    console.log(e.currentTarget);
    e.stopPropagation();
    removeActiveMenuClass();
    $(this).addClass('active');
});

function removeActiveMenuClass(){
    $('.menu li').removeClass('active');
}
