<!doctype html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="MobileOptimized" content="320">

    <title>Полуфабрикаты ДО100ТОЧНО</title>

    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
{{--    <link rel="stylesheet" href="css/slick.css">--}}
    <link rel="icon" href="img/icon.png">
    <link rel="stylesheet" href="css/style.css">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


</head>
<body>
<div id="wrap">
    <!-- header -->
    <header>
        <div class="container">
            <div class="d-flex align-item-center">
                <a href="/" class="logo"><img src="img/logo.svg" alt="" class="img-responsive"></a>
                <!-- navigation -->
                <nav>
                    <div id="navbar" class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li><a href="#catalogue">ПРОДУКЦИЯ</a></li>
                            <li><a href="#about">О НАС</a></li>
                            <li><a href="#contacts">АДРЕСА МАГАЗИНОВ</a></li>
                            <li><a href="https://www.instagram.com/do100tochno.tlt/" target="_blank">МЫ В INSTAGRAM</a></li>
                            <li><a href="https://vk.com/do100tochno1" target="_blank">VK</a></li>
                        </ul>
                    </div>
                </nav>
                <!-- /navigation -->
                <a href="#callback" class="btn" data-toggle="modal">Связаться с нами</a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
        </div>
        @if(session('submitted'))
        <div class="container text-center">
        <p style="color: green">
            {{session('submitted')}}
        </p>
        </div>
        @endif
        @if(session('error'))
        <div class="container text-center">
        <p style="color: red">
            {{session('error')}}
        </p>
        </div>
        @endif
    </header>
    <!-- /header -->
    <!-- preview -->
    <div class="preview">
        <div class="container text-center">
            <img src="img/logo-white.svg" alt="" class="img-responsive center-block">
            <h1 class="title text-bold">Производство и продажа</h1>
        </div>
    </div>
    <!-- /preview -->
    <!-- catalogue -->
    <div class="catalogue" id="catalogue">
        <div class="catalogue-left"></div>
        <div class="catalogue-right"></div>
        <div class="container">
            <div class="row">
                @foreach($products as $item)
                <div class="col-sm-6 col-lg-4">
                    <div class="item-block">
                        <div class="item-block-image">
                            <picture>
{{--                                <source data-srcset="img/content/item-1.webp" type="image/webp">--}}
{{--                                <source data-srcset="img/content/item-2.jpg" type="image/jpeg">--}}
{{--                                <img alt="Биточки добрые" class="lazyload" data-src="img/content/item-1.jpg">--}}
                                <img src="{{ asset('storage/' . $item->product_photo) }}" />

                            </picture>
                        </div>
                        <div class="item-block-name"><h3 class="title-md text-bold">{{$item->product_name}}</h3></div>
                    </div>
                </div>
            @endforeach
{{--                <div class="col-sm-6 col-lg-4">--}}
{{--                    <div class="item-block">--}}
{{--                        <div class="item-block-name"><h3 class="title-md text-bold">Блинчики</h3></div>--}}
{{--                        <div class="item-block-image">--}}
{{--                            <picture>--}}
{{--                                <source data-srcset="img/content/item-2.webp" type="image/webp">--}}
{{--                                <source data-srcset="img/content/item-2.jpg" type="image/jpeg">--}}
{{--                                <img alt="Блинчики" class="lazyload" data-src="img/content/item-2.jpg">--}}
{{--                            </picture>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-sm-6 col-lg-4">--}}
{{--                    <div class="item-block">--}}
{{--                        <div class="item-block-name"><h3 class="title-md text-bold">{{$products->product_name}}</h3></div>--}}
{{--                        <div class="item-block-image">--}}
{{--                            <picture>--}}
{{--                                <source data-srcset="{{$products->product_photo}}" type="image/webp">--}}
{{--                                <source data-srcset="{{$products->product_photo}}" type="image/jpeg">--}}
{{--                                <img alt="Блинчики" class="lazyload" data-src="{{$products->product_photo}}">--}}
{{--

{{--                            </picture>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-6 col-lg-4">--}}
{{--                    <div class="item-block">--}}
{{--                        <div class="item-block-image">--}}
{{--                            <picture>--}}
{{--                                <source data-srcset="img/content/item-3.webp" type="image/webp">--}}
{{--                                <source data-srcset="img/content/item-3.jpg" type="image/jpeg">--}}
{{--                                <img alt="Узелки" class="lazyload" data-src="img/content/item-3.jpg">--}}
{{--                            </picture>--}}
{{--                        </div>--}}
{{--                        <div class="item-block-name"><h3 class="title-md text-bold">Узелки</h3></div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-6 col-lg-4">--}}
{{--                    <div class="item-block">--}}
{{--                        <div class="item-block-image">--}}
{{--                            <picture>--}}
{{--                                <source data-srcset="img/content/item-4.webp" type="image/webp">--}}
{{--                                <source data-srcset="img/content/item-4.jpg" type="image/jpeg">--}}
{{--                                <img alt="Пельмени" class="lazyload" data-src="img/content/item-4.jpg">--}}
{{--                            </picture>--}}
{{--                        </div>--}}
{{--                        <div class="item-block-name"><h3 class="title-md text-bold">Пельмени</h3></div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-6 col-lg-4">--}}
{{--                    <div class="item-block">--}}
{{--                        <div class="item-block-name"><h3 class="title-md text-bold">Котлеты двинские</h3></div>--}}
{{--                        <div class="item-block-image">--}}
{{--                            <picture>--}}
{{--                                <source data-srcset="img/content/item-5.webp" type="image/webp">--}}
{{--                                <source data-srcset="img/content/item-5.jpg" type="image/jpeg">--}}
{{--                                <img alt="Котлеты двинские" class="lazyload" data-src="img/content/item-5.jpg">--}}
{{--                            </picture>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-6 col-lg-4">--}}
{{--                    <div class="item-block">--}}
{{--                        <div class="item-block-image">--}}
{{--                            <picture>--}}
{{--                                <source data-srcset="img/content/item-6.webp" type="image/webp">--}}
{{--                                <source data-srcset="img/content/item-6.jpg" type="image/jpeg">--}}
{{--                                <img alt="Вареники" class="lazyload" data-src="img/content/item-6.jpg">--}}
{{--                            </picture>--}}
{{--                        </div>--}}
{{--                        <div class="item-block-name"><h3 class="title-md text-bold">Вареники</h3></div>--}}
                    </div>
                </div>
            </div>
{{--            <div class="text-center catalogue-btn"><a href="#" class="btn">скачать всю продукцию</a></div>--}}
        </div>

    <!-- /catalogue -->
    <!-- natural -->
    <section class="natural">
        <div class="container">
            <h2 class="title text-center"><span class="text-bold">Натуральные полуфабрикаты</span> <br><i>на каждый день</i></h2>
        </div>
    </section>
    <!-- /natural -->
    <!-- about -->

    <div class="about" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 order-lg-2">
                    <div class="about-text">
                        <div class="title text-bold">О нас</div>
                        <p>ООО «Успех» было основано в 2019 году в г. Тольятти, имеется современное производство, которое беспрерывно функционирует на территории Автозаводского района. На сегодняшний день компания «Успех» успешно сочетает большие объёмы производства и высокое качество продукции. Основным ассортиментом нашей компании являются замороженные полуфабрикаты и кондитерские изделия быстрого приготовления. Ассортимент нашей продукции постоянно расширяется благодаря профессионализму коллектива технологов.</p>
                        <p>Под брендом «До100Точно» работают как собственные торговые точки, так и отделы в магазинах партнеров. Все они оформлены в едином корпоративном стиле бренда. Бренд «До100Точно» официально зарегистрирован, в основе него лежит доступность по ценовому параметру и высокое качество выпускаемой продукции.</p>
{{--                        <p><a href="#" class="text-grey">Читать дальше</a></p>--}}
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="about-image">
                        <picture>
                            <source data-srcset="img/about.webp" type="image/webp">
                            <source data-srcset="img/about.jpg" type="image/jpeg">
                            <img alt="" class="lazyload img-responsive" data-src="img/about.jpg">
                        </picture>
                    </div>
                    <p class="text-grey about-text"><i>Вся выпускаемая продукция прошла необходимые сертификационные и разрешительные процедуры, разработаны собственные технические условия на весь ассортимент, производимый компанией.</i></p></figcaption>
                </div>
            </div>
        </div>
    </div>
    <!-- /about -->
    <!-- ins-section -->
    <div class="ins-section">
        <div class="container">
            <div class="title text-center"><span class="text-bold">Наш</span> <i>Instagram</i></div>
        </div>
        <div class="ins-slider">
            <div>
                <a href="img/content/ins-1.jpg" class="zoom-link" data-fancybox="ins-slider">
                    <picture>
                        <source data-srcset="img/content/ins-1.webp" type="image/webp">
                        <source data-srcset="img/content/ins-1.jpg" type="image/jpeg">
                        <img alt="" class="lazyload" data-src="img/ins-1.jpg">
                    </picture>
                </a>
            </div>
            <div>
                <a href="img/content/ins-2.jpg" class="zoom-link" data-fancybox="ins-slider">
                    <picture>
                        <source data-srcset="img/content/ins-2.webp" type="image/webp">
                        <source data-srcset="img/content/ins-2.jpg" type="image/jpeg">
                        <img alt="" class="lazyload" data-src="img/ins-2.jpg">
                    </picture>
                </a>
            </div>
            <div>
                <a href="img/content/ins-3.jpg" class="zoom-link" data-fancybox="ins-slider">
                    <picture>
                        <source data-srcset="img/content/ins-3.webp" type="image/webp">
                        <source data-srcset="img/content/ins-3.jpg" type="image/jpeg">
                        <img alt="" class="lazyload" data-src="img/ins-3.jpg">
                    </picture>
                </a>
            </div>
            <div>
                <a href="img/content/ins-4.jpg" class="zoom-link" data-fancybox="ins-slider">
                    <picture>
                        <source data-srcset="img/content/ins-4.webp" type="image/webp">
                        <source data-srcset="img/content/ins-4.jpg" type="image/jpeg">
                        <img alt="" class="lazyload" data-src="img/ins-.jpg">
                    </picture>
                </a>
            </div>
            <div>
                <a href="img/content/ins-5.jpg" class="zoom-link" data-fancybox="ins-slider">
                    <picture>
                        <source data-srcset="img/content/ins-5.webp" type="image/webp">
                        <source data-srcset="img/content/ins-5.jpg" type="image/jpeg">
                        <img alt="" class="lazyload" data-src="img/ins-5.jpg">
                    </picture>
                </a>
            </div>
            <div>
                <a href="img/content/ins-6.jpg" class="zoom-link" data-fancybox="ins-slider">
                    <picture>
                        <source data-srcset="img/content/ins-6.webp" type="image/webp">
                        <source data-srcset="img/content/ins-6.jpg" type="image/jpeg">
                        <img alt="" class="lazyload" data-src="img/ins-6.jpg">
                    </picture>
                </a>
            </div>
        </div>
    </div>
    <!-- /ins-section -->
    <!-- popular -->
    <div class="popular">
        <div class="container">
            <div class="title text-center"><span class="text-bold">Самые продаваемые</span> <br><i class="text-red">наши продукты</i></div>
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <div class="item-block">
                        <div class="item-block-image">
                            <picture>
                                <source data-srcset="img/content/item-7.webp" type="image/webp">
                                <source data-srcset="img/content/item-7.jpg" type="image/jpeg">
                                <img alt="Манты по узбекски" class="lazyload" data-src="img/content/item-7.jpg">
                            </picture>
                        </div>
                        <div class="item-block-name"><h3 class="title-md text-bold">Манты по узбекски</h3></div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="item-block">
                        <div class="item-block-name"><h3 class="title-md text-bold">Пельмени улетные</h3></div>
                        <div class="item-block-image">
                            <picture>
                                <source data-srcset="img/content/item-8.webp" type="image/webp">
                                <source data-srcset="img/content/item-8.jpg" type="image/jpeg">
                                <img alt="Пельмени улетные" class="lazyload" data-src="img/content/item-8.jpg">
                            </picture>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="item-block">
                        <div class="item-block-image">
                            <picture>
                                <source data-srcset="img/content/item-9.webp" type="image/webp">
                                <source data-srcset="img/content/item-9.jpg" type="image/jpeg">
                                <img alt="Самса" class="lazyload" data-src="img/content/item-9.jpg">
                            </picture>
                        </div>
                        <div class="item-block-name"><h3 class="title-md text-bold">Самса</h3></div>
                    </div>
                </div>
            </div>
{{--            <div class="text-center catalogue-btn"><a href="#" class="btn">скачать всю продукцию</a></div>--}}
        </div>
    </div>
    <!-- /popular -->
    <!-- gallery -->
    <div class="gallery">
        <div class="container">
            <div class="gallery-list">
                <a href="img/content/gallery-1.jpg" class="zoom-link gallery-col" data-fancybox="gallery">
                    <picture>
                        <source data-srcset="img/content/gallery-1.webp" type="image/webp">
                        <source data-srcset="img/content/gallery-1.jpg" type="image/jpeg">
                        <img alt="" class="lazyload" data-src="img/gallery-1.jpg">
                    </picture>
                </a>
                <a href="img/content/gallery-2.jpg" class="zoom-link gallery-row" data-fancybox="gallery">
                    <picture>
                        <source data-srcset="img/content/gallery-2.webp" type="image/webp">
                        <source data-srcset="img/content/gallery-2.jpg" type="image/jpeg">
                        <img alt="" class="lazyload" data-src="img/gallery-2.jpg">
                    </picture>
                </a>
                <a href="img/content/gallery-3.jpg" class="zoom-link" data-fancybox="gallery">
                    <picture>
                        <source data-srcset="img/content/gallery-3.webp" type="image/webp">
                        <source data-srcset="img/content/gallery-3.jpg" type="image/jpeg">
                        <img alt="" class="lazyload" data-src="img/gallery-3.jpg">
                    </picture>
                </a>
                <a href="img/content/gallery-4.jpg" class="zoom-link gallery-row" data-fancybox="gallery">
                    <picture>
                        <source data-srcset="img/content/gallery-4.webp" type="image/webp">
                        <source data-srcset="img/content/gallery-4.jpg" type="image/jpeg">
                        <img alt="" class="lazyload" data-src="img/gallery-4.jpg">
                    </picture>
                </a>
                <a href="img/content/gallery-5.jpg" class="zoom-link" data-fancybox="gallery">
                    <picture>
                        <source data-srcset="img/content/gallery-5.webp" type="image/webp">
                        <source data-srcset="img/content/gallery-5.jpg" type="image/jpeg">
                        <img alt="" class="lazyload" data-src="img/gallery-5.jpg">
                    </picture>
                </a>
                <a href="img/content/gallery-6.jpg" class="zoom-link" data-fancybox="gallery">
                    <picture>
                        <source data-srcset="img/content/gallery-6.webp" type="image/webp">
                        <source data-srcset="img/content/gallery-6.jpg" type="image/jpeg">
                        <img alt="" class="lazyload" data-src="img/gallery-6.jpg">
                    </picture>
                </a>
            </div>
{{--            <div class="text-center catalogue-btn"><a href="#" class="btn">ПОКАЗАТь ЕЩЕ</a></div>--}}
        </div>
    </div>
    <!-- gallery -->
    <!-- products -->
    <section class="products">
        <div class="container">
            <h2 class="title text-center"><span class="text-bold">Ваша сила —  </span> <i>в наших продуктах</i></h2>
        </div>
    </section>
    <!-- /products -->
    <!-- contacts -->
    <div class="contacts" id="contacts">
        <div class="container">
            <div class="title text-center"><span class="text-bold">Мы находимся</span> <br><i class="text-red">в Тольятти</i></div>
            <div class="row m-0">
                @foreach($data as $item)
                <div class="col-sm-6 col-lg-4 contacts-item"><span class="i-map"></span> <br>{{$item->address}}</div>
                @endforeach
{{--                <div class="col-sm-6 col-lg-4 contacts-item"><span class="i-map"></span>Т Восход, <br>Революционная  20</div>--}}
{{--                <div class="col-sm-6 col-lg-4 contacts-item"><span class="i-map"></span>Т Десятка, Степана <br>Разина 9A</div>--}}
{{--                <div class="col-sm-6 col-lg-4 contacts-item"><span class="i-map"></span>Т Детский Мир, <br>Степана Разина 60</div>--}}
{{--                <div class="col-sm-6 col-lg-4 contacts-item"><span class="i-map"></span>Т Журавль, <br>Мира 56</div>--}}
{{--                <div class="col-sm-6 col-lg-4 contacts-item"><span class="i-map"></span>Т Захар, Королева 18а</div>--}}
{{--                <div class="col-sm-6 col-lg-4 contacts-item"><span class="i-map"></span>Т Изюм, <br>Свердлова 3Б</div>--}}
{{--                <div class="col-sm-6 col-lg-4 contacts-item"><span class="i-map"></span>Т Каретный, <br>Тополиная 24A</div>--}}
{{--                <div class="col-sm-6 col-lg-4 contacts-item"><span class="i-map"></span>Т Кунеевский, <br>Лизы Чайкиной 50A</div>--}}
{{--                <div class="col-sm-6 col-lg-4 contacts-item"><span class="i-map"></span>Т Ленинградская 53A</div>--}}
{{--                <div class="col-sm-6 col-lg-4 contacts-item"><span class="i-map"></span>Т Журавль, <br>Мира 67</div>--}}
{{--                <div class="col-sm-6 col-lg-4 contacts-item"><span class="i-map"></span>Т Николаевский, <br>Автостроителей, 68A</div>--}}
{{--                <div class="col-sm-6 col-lg-4 contacts-item"><span class="i-map"></span>Т Новинка, <br>Автостроителей 80 А к1</div>--}}
{{--                <div class="col-sm-6 col-lg-4 contacts-item"><span class="i-map"></span>Т Сорренто-Оптима, <br>Свердлова 15</div>--}}
{{--                <div class="col-sm-6 col-lg-4 contacts-item"><span class="i-map"></span>Т Ст.торговый, <br>Революционная  28/1</div>--}}
{{--                <div class="col-sm-6 col-lg-4 contacts-item"><span class="i-map"></span>Т Телецентр, <br>Степана Разина 26 А</div>--}}
{{--                <div class="col-sm-6 col-lg-4 contacts-item"><span class="i-map"></span>Т Туполева, <br>Туполева 3а</div>--}}
            </div>
        </div>
    </div>

    <div class="map">
        <div class="container">
            <div class="address">
                <a href="#" data-toggle="dropdown">Выберите адрес</a>
                <ul class="dropdown-menu menu" role="menu" id="mapMenu">

                    <li id="place1" class="active">Горького 92 а</li>
                    <li id="place2">Революционная  20</li>
                    <li id="place3">Степана Разина 9A</li>
                    <li id="place4">Степана Разина 60</li>
                    <li id="place5">Мира 56</li>
                    <li id="place6">Королева 18а</li>
                    <li id="place7">Свердлова 3Б</li>
                    <li id="place8">Тополиная 24A</li>
                    <li id="place9">Лизы Чайкиной 50A</li>
                    <li id="place10">Т Ленинградская 53A</li>
                    <li id="place11">Мира 67</li>
                    <li id="place12">Автостроителей, 68A</li>
                    <li id="place18">Автостроителей 80 А к1</li>
                    <li id="place14">Свердлова 15</li>
                    <li id="place15">Революционная  28/1</li>
                    <li id="place16">Степана Разина 26 А</li>
                    <li id="place17">Туполева 3а</li>
                </ul>
            </div>
        </div>
        <div id="map"></div>
    </div>
    <!-- /contacts -->
    <!-- footer -->
    <footer>
        <div class="container">
            <div class="d-flex align-item-center">
                <a href="/" class="logo"><img src="img/logo.svg" alt=""></a>
                <!-- navigation -->
                <nav>
                    <ul class="nav navbar-nav">
                        <li><a href="#catalogue">ПРОДУКЦИЯ</a></li>
                        <li><a href="#about">О НАС</a></li>
                        <li><a href="#contacts">АДРЕСА МАГАЗИНОВ</a></li>
                        <li><a href="https://www.instagram.com/do100tochno.tlt/" target="_blank">МЫ В INSTAGRAM</a></li>
                        <li><a href="https://vk.com/do100tochno1" target="_blank">VK</a></li>
                    </ul>
                </nav>
                <!-- /navigation -->
                <a href="#callback" class="btn" data-toggle="modal">Связаться с нами</a>
            </div>
        </div>

    </footer>
    <!-- /footer -->
</div>

<!-- callback -->
<div class="modal fade" id="callback" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-title" >Связаться с нами</div>
                <form action="/" method="POST" id="contact" >
                    @csrf
                    <div class="form-group"><input type="text" placeholder="Имя" name="name" required>
                    <span style="color: #ff0000">@error('name'){{$message}}@enderror</span>
                    </div>
                    <div class="form-group"><input type="text" placeholder="Телефон" name="phone" required>
                        <span style="color: red">@error('phone'){{$message}}@enderror</span>
                    </div>
                    <div class="form-group"><input type="email" placeholder="Имейл" name="email" required >
                        <span style="color: red">@error('email'){{$message}}@enderror</span>
                    </div>
{{--                    <button type="submit" class="btn btn-default btn-block" id="save">отправить</button>--}}
                    <input type="submit" name="submit" class="btn btn-default btn-block" id="save" value="Отправить"  ">
                </form>

            </div>
        </div>
    </div>
</div>
<!-- /callback -->


<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/lazysizes.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.fancybox.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="https://api-maps.yandex.ru/2.1/?apikey=3732126d-6a5c-42cc-8652-ade22af8cabe&lang=ru_RU"></script>
<script src="js/main.js"></script>
{{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">--}}

</body>
</html>
