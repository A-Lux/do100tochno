@section('about_us')
<div class="about" id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 order-lg-2">
                <div class="about-text">
                    <div class="title text-bold">О нас</div>
                    <p>ООО «Успех» было основано в 2019 году в г. Тольятти, имеется современное производство, которое беспрерывно функционирует на территории Автозаводского района. На сегодняшний день компания «Успех» успешно сочетает большие объёмы производства и высокое качество продукции. Основным ассортиментом нашей компании являются замороженные полуфабрикаты и кондитерские изделия быстрого приготовления. Ассортимент нашей продукции постоянно расширяется благодаря профессионализму коллектива технологов.</p>
                    <p>Под брендом «До100Точно» работают как собственные торговые точки, так и отделы в магазинах партнеров. Все они оформлены в едином корпоративном стиле бренда. Бренд «До100Точно» официально зарегистрирован, в основе него лежит доступность по ценовому параметру и высокое качество выпускаемой продукции.</p>
                    <p><a href="#" class="text-grey">Читать дальше</a></p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="about-image">
                    <picture>
                        <source data-srcset="img/about.webp" type="image/webp">
                        <source data-srcset="img/about.jpg" type="image/jpeg">
                        <img alt="" class="lazyload img-responsive" data-src="img/about.jpg">
                    </picture>
                </div>
                <p class="text-grey about-text"><i>Вся выпускаемая продукция прошла необходимые сертификационные и разрешительные процедуры, разработаны собственные технические условия на весь ассортимент, производимый компанией.</i></p></figcaption>
            </div>
        </div>
    </div>
</div>
@endsection
