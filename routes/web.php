<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Models\Contact;
use App\Models\Image;
use Illuminate\Support\Facades\Validator;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $data = \App\Models\Address::all();
    $products = \App\Models\Product::all();
    $contacts = \App\Models\Contact::all();
    return view('welcome',['products'=>$products],['data'=>$data]);
});
Route::post('/',function (Request $request){
//   $validate =  $request->validate([
//        'name'=>'required|string',
//        'email'=>'required|email',
//        'phone'=>'required|numeric'
//    ]);
    $validate = Validator::make($request->all(),[
        'name'=>'required|string',
        'email'=>'required|email',
        'phone'=>'required|numeric'
    ]);
   if ($validate->fails()) {
       return redirect()->back()->with('error', 'Произошла ошибка, убедитесь что все поля корректно заполнены ');
   }
else{
       $data = new Contact();
       $data->name = $request->name;
       $data->email = $request->email;
       $data->phone = $request->phone;
       $data->save();
       return redirect()->back()->with('submitted', 'Ваша заявка принята ');

   }
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
